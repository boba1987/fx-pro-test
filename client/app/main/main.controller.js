'use strict';

(function() {

  class MainController {

    constructor($http, $scope, socket) {
      this.$http = $http;
      this.socket = socket;
      this.awesomeChats = [];

      $scope.$on('$destroy', function() {
        socket.unsyncUpdates('chat');
      });
    }

    $onInit() {
      this.$http.get('/api/chats')
        .then(response => {
          this.awesomeChats = response.data;
          this.socket.syncUpdates('chat', this.awesomeChats);
        });
    }

    deleteChat(chat) {
      this.$http.delete('/api/chats/' + chat._id);
    }
  }

  angular.module('fxProChatApp')
    .component('main', {
      templateUrl: 'app/main/main.html',
      controller: MainController
    });
})();
