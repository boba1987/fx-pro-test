'use strict';

angular.module('fxProChatApp', ['fxProChatApp.constants', 'ngCookies', 'ngResource', 'ngSanitize',
    'btford.socket-io', 'ui.router', 'ui.bootstrap'
  ])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(true);
  })
  .run(function($rootScope, Modal, storage){

    storage.bootstrap();

    // If no fx-userName in localStorage, open modal
    if(!storage.get('userName')){
      var modal = Modal.welcome.confirm();
      modal();
    }else{
      $rootScope.userName = storage.get('userName');
    }
  });
