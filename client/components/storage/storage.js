'use strict';

(function () {
    angular.module('fxProChatApp')
        .factory('storage', storage);

    function storage(
        $window
    ) {
        var storageObject = {
            // Declare all possible storage objects (they are used for bootstrapping)
            userName: null,
        };
        var prefix = 'fx-';
        var localStorageSupported = checkLocalStorage();

        var storageFactory = {
            save: save,
            remove: remove,
            get: get,
            bootstrap: bootstrap
        };

        return storageFactory;

        function checkLocalStorage() {
            try {
                $window.localStorage.setItem('fx.checkLocalStorage', '1');
                $window.localStorage.removeItem('fx.checkLocalStorage');
                return true;
            } catch (e) {
                return false;
            }
        }

        function bootstrap() {
            for (var key in storageObject) {
                if (storageObject.hasOwnProperty(key)) {
                    var value = get(key, true);
                    storageObject[key] = value;
                }
            }
        }

        function save(key, value, noPersistency) {
            storageObject[key] = value;
            if (noPersistency) {
                // Use noPersistency if object should only be saved for
                // the duration of the session
                return;
            }
            if (localStorageSupported) {
                $window.localStorage.setItem(prefix + key, angular.toJson(value));
            } else {
                // Cookie has a limit of less than ~4kb of data
                // Warn if cookie size over 3kb
                var cookieValue = angular.toJson(value);
                if (cookieValue.length > 3000) {
                    console.error('Warning, cookie storage near or over the limit');
                }
            }
        }

        function remove(key) {
            if (localStorageSupported) {
                $window.localStorage.removeItem(prefix + key);
            }
            // Don't delete keys since they might be needed for bootstrap
            storageObject[key] = null;
        }

        function get(key, bootstrap) {
          // Only return from localStorage/cookies during bootstrap
          // Use in-memory storage instead
            if (bootstrap) {
                if (localStorageSupported) {
                    return angular.fromJson($window.localStorage.getItem(prefix + key));
                }
            }
            return storageObject[key];
        }
    }
})();
