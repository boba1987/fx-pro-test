'use strict';

angular.module('fxProChatApp')
  .directive('chatForm', function() {
    return {
      templateUrl: 'components/form/form.template.html',
      restrict: 'E'
    };
  });
