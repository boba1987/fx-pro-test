/* global io */
'use strict';

(function() {

  class FormController {
    constructor($http, socket, $scope, $rootScope) {
      this.$rootScope = $rootScope;
      this.$http = $http;
      this.socket = socket;
      this.awesomeChats = [];

      $scope.$on('$destroy', function() {
        socket.unsyncUpdates('chat');
      });
    }

    addChat(newChat) {
      if (newChat) {
        this.$http.post('/api/chats', newChat);
        this.newChat = '';
      }
    }

    sendMsg(msg){
      var msgObj = {
        user: this.$rootScope.userName || 'unknow user',
        message: msg,
        time: new Date()
      };

      this.addChat(msgObj);

      var socket = io();
      socket.emit('chat message', msgObj);
      this.chatMsg = '';
    }

    $onInit() {
      this.chatMsg = '';
      this.msgs = [];
    }
  }

  angular.module('fxProChatApp')
    .component('chat', {
      templateUrl: 'components/form/form.template.html',
      controller: FormController,
      bindings: {
        chats: '='
      }
    });
})();
