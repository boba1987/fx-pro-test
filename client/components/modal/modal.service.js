'use strict';

angular.module('fxProChatApp')
  .factory('Modal', function($rootScope, $uibModal, storage) {
    /**
     * Opens a modal
     * @param  {Object} scope      - an object to be merged with modal's scope
     * @param  {String} modalClass - (optional) class(es) to be applied to the modal
     * @return {Object}            - the instance $uibModal.open() returns
     */
    function openModal(scope = {}, modalClass = 'modal-default', template) {
      var modalScope = $rootScope.$new();

      angular.extend(modalScope, scope);

      return $uibModal.open({
        templateUrl: template,
        windowClass: modalClass,
        scope: modalScope
      });
    }

    // Public API here
    return {

      /* Confirmation modals */
      confirm: {

        /**
         * Create a function to open a delete confirmation modal (ex. ng-click='myModalFn(name, arg1, arg2...)')
         * @param  {Function} del - callback, ran when delete is confirmed
         * @return {Function}     - the function to open the modal (ex. myModalFn)
         */
        delete(del = angular.noop) {
          /**
           * Open a delete confirmation modal
           * @param  {String} name   - name or info to show on modal
           * @param  {All}           - any additional args are passed straight to del callback
           */
          return function() {
            var args = Array.prototype.slice.call(arguments),
              name = args.shift(),
              deleteModal;

            deleteModal = openModal({
              modal: {
                dismissable: true,
                title: 'Confirm Delete',
                html: '<p>Are you sure you want to delete <strong>' + name +
                  '</strong> ?</p>',
                buttons: [{
                  classes: 'btn-danger',
                  text: 'Delete',
                  click: function(e) {
                    deleteModal.close(e);
                  }
                }, {
                  classes: 'btn-default',
                  text: 'Cancel',
                  click: function(e) {
                    deleteModal.dismiss(e);
                  }
                }]
              }
            }, 'modal-danger', 'components/modal/modal.html');

            deleteModal.result.then(function(event) {
              del.apply(event, args);
            });
          };
        }
      },

      /* Welcome modals */
      welcome: {
        /**
         * Create a function to open a delete welcome modal (ex. ng-click='myModalFn(name, arg1, arg2...)')
         * @param  {Function} confirm - callback, ran when user name is confirmed
         * @return {Function}     - the function to open the modal (ex. myModalFn)
         */

         confirm(confirm = angular.noop){

           return function(){
             var args = Array.prototype.slice.call(arguments);
             var openWelcomeModal;

             openWelcomeModal = openModal({
               modal: {
                 title: 'Welcome User! Please, enter your User name to chat!',
                 buttons: [{
                   classes: 'btn-primary',
                   text: 'Confirm user name',
                   click: function(userName) {
                     $rootScope.userName = userName;
                     openWelcomeModal.dismiss();

                     storage.save('userName', userName);
                   }
                 }]
               }
             }, 'modal-danger', 'components/modal/modal.welcome.html');

             openWelcomeModal.result.then(function(event) {
               confirm.apply(event, args);
             });
           };
         }
      }
    };
  });
