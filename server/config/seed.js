/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Chat from '../api/chat/chat.model';

Chat.find({}).remove()
  .then(() => {
    Chat.create({
      user: 'Bot User',
      message: 'Hello there! You can start your chat now!',
      time: new Date()
    });
  });
