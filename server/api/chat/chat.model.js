'use strict';

import mongoose from 'mongoose';

var ChatSchema = new mongoose.Schema({
  name: String,
  user: String,
  message: String,
  time: Date,
  active: Boolean
});

export default mongoose.model('Chat', ChatSchema);
